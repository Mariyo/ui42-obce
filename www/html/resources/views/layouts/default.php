<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= __('Vyhladavanie v databaze obci') ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?= asset('/css/app.css'); ?>" rel="stylesheet">
</head>

<body>

<div id="app">
    <div class="navbar navbar-inverse bg-inverse">
        <div class="container d-flex justify-content-between">
            <a href="<?= route('\App\Http\Controllers\PlacesController@search') ?>"
               class="navbar-brand"><?= __('Uvodna stranka') ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>

    <section class="jumbotron text-center">
        <div class="container">
            <?= $content ?>
        </div>
    </section>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>Footer menu placeholder &copy; Bootstrap!</p>
        </div>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="<?= asset('/js/app.js'); ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  //Javascript
  $(function() {
    $('#q').autocomplete({
      source: '/search/autocomplete',
      minLength: 1,
      select: function(event, ui) {
        window.location.replace('/place/' + ui.item.id);
      },
    });
  });
</script>
</body>
</html>