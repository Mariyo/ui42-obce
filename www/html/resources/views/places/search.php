<h1 class="jumbotron-heading"><?= __('Vyhladat v databaze obci') ?></h1>
<div class="row align-items-center justify-content-center">
    <div class="col-4">
        <?= Form::open(['action' => '\App\Http\Controllers\PlacesController@search', 'class' => 'lead']) ?>
        <?= Form::text('q', '', ['class' => 'form-control', 'id' => 'q']) ?>
        <?= Form::close() ?>
    </div>
</div>