<h1 class="jumbotron-heading"><?= __('Detail obce') ?></h1>
<div class="row align-items-center justify-content-center">
    <div class="col-6">
        <table>
            <tr>
                <th><?= __('Meno starostu:') ?></th>
                <td><?= $place->mayor ?></td>
            </tr>
            <tr>
                <th><?= __('Zdroj:') ?></th>
                <td><?= $place->source ?></td>
            </tr>
        </table>
    </div>
    <div class="col-6">
        <img src="https://www.techonline.com/img/tmp/logo-placeholder.png" alt="">
        <h2><?= $place->name ?></h2>
    </div>
</div>