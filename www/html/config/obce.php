<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site strategy
    |--------------------------------------------------------------------------
    |
    | Define strategy, which we will use data import.
    |
    */
    'sourceStrategy' => \App\Data\Source\Strategies\EObce::class,

    /*
    |--------------------------------------------------------------------------
    | Limit per one import
    |--------------------------------------------------------------------------
    |
    | Define number of places imported per one data import.
    |
    */
    'limit' => 5,
];