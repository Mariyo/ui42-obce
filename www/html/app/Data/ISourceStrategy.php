<?php

namespace App\Data;

interface ISourceStrategy
{
    public function listing();

    public function places(int $limit);
}