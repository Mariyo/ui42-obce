<?php

namespace App\Data\Source\Strategies;

use App\Data\ISourceStrategy;
use App\Entities\Place;
use Illuminate\Database\Eloquent\Collection;
use simplehtmldom_1_5\simple_html_dom;
use Sunra\PhpSimple\HtmlDomParser;

class EObce implements ISourceStrategy
{
    protected $url = 'https://www.e-obce.sk/zoznam_vsetkych_obci.html?strana=';

    /**
     *
     */
    public function listing()
    {
        $page = 0;
        do {
            $html = $this->getHtml($this->url . $page);
            $content = HtmlDomParser::str_get_html($html);
            $places = $this->getPlaces($content);
            $page += 500;
        } while ($places > 0 and $page < 5000);
    }

    /**
     * @param int $limit
     */
    public function places(int $limit)
    {
        /** @var $places Collection */
        $places = Place::where('imported', 0)->take($limit)->get();
        if ($places->count() == 0) {
            return;
        }

        foreach ($places as $place) {
            $html = $this->getHtml($place->source);
            $content = HtmlDomParser::str_get_html($html);

            foreach ($content->find('td') as $element) {
                if ($element->text() == 'Starosta:') {
                    $place->mayor = $element->nextSibling()->text();
                    break;
                }
            }

            foreach ($content->find('h1') as $element) {
                $place->name = $element->text();
            }

            $place->imported = 1;
            $place->save();
        }
    }

    /**
     * @param simple_html_dom $content
     * @return int
     */
    protected function getPlaces(simple_html_dom $content)
    {
        $places = 0;

        foreach ($content->find('a') as $element) {
            if (empty($element->href) or !preg_match('~^https://www.e-obce.sk/obec/~', $element->href)) {
                continue;
            }

            /** @var $results Collection */
            $results = Place::where('source', $element->href)->get();
            if ($results->count() > 0) {
                continue;
            }

            $place = new Place();
            $place->name = '';
            $place->slug = '';
            $place->postcode = '';
            $place->mayor = '';
            $place->address = '';
            $place->imported = 0;
            $place->source = $element->href;
            $place->save();

            $places++;
        }

        return $places;
    }

    protected function getHtml($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        return curl_exec($curl);
    }
}