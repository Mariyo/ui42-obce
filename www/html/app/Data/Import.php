<?php
/**
 * Created by PhpStorm.
 * User: mariyo
 * Date: 3.4.2018
 * Time: 22:08
 */

namespace App\Data;


class Import
{

    /**
     * @var ISourceStrategy
     */
    protected $source;

    public function __construct(ISourceStrategy $source)
    {
        $this->source = $source;
    }

    /**
     * Import listing of places for future import
     */
    public function listing()
    {
        $this->source->listing();
    }

    /**
     * Import next places
     * @param int $limit
     */
    public function places(int $limit)
    {
        $this->source->places($limit);
    }
}