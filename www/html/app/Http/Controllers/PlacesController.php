<?php

namespace App\Http\Controllers;

use App\Entities\Place;
use Illuminate\Support\Facades\Input;

class PlacesController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        return view('layouts/default', [
            'content' => view('places.search'),
            'title' => __('Vyhladavanie v databaze obci')
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocomplete()
    {
        $places = Place::where('imported', 1)
            ->where(function ($query) {
                $query
                    ->where('name', 'like', '%' . Input::get('term') . '%')
                    ->orWhere('mayor', 'like', '%' . Input::get('term') . '%');
            })
            ->take(5)
            ->get();

        $response = [];
        foreach ($places as $place) {
            $response[] = [
                'id' => $place->id,
                'value' => $place->name
            ];
        }

        return response()->json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $place = Place::where('imported', 1)->find($id);
        if (empty($place)) {
            abort(404);
        }

        return view('layouts/default', [
            'content' => view('places.detail', [
                'place' => $place,
            ]),
            'title' => __('Detail obce'),
        ]);
    }
}
