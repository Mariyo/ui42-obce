<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'postcode',
        'mayor',
        'address',
        'imported',
        'source',
    ];
}
