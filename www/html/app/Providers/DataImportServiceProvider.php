<?php

namespace App\Providers;

use App\Data\Import;
use Illuminate\Support\ServiceProvider;

class DataImportServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Import::class, function () {
            $sourceStrategy = config('obce.sourceStrategy');
            return new Import(new $sourceStrategy());
        });
    }
}
