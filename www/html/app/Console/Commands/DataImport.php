<?php

namespace App\Console\Commands;

use App\Data\Import;
use Illuminate\Console\Command;

class DataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import {limit=0 : limit per import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data';

    /**
     * The console command description.
     *
     * @var Import
     */
    protected $import;

    /**
     * Create a new command instance.
     *
     * @param Import $import
     */
    public function __construct(Import $import)
    {
        parent::__construct();
        $this->import = $import;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = (int)$this->argument('limit');
        $this->import->listing();
        $this->import->places(!empty($limit) ? $limit : config('obce.limit'));
    }
}
