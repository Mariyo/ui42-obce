<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});*/
\Miroc\LaravelAdminer\AdminerAutologinController::class;
Route::get('/', '\App\Http\Controllers\PlacesController@search')->name('\App\Http\Controllers\PlacesController@search');
Route::get('/search/autocomplete',
    '\App\Http\Controllers\PlacesController@autocomplete')->name('\App\Http\Controllers\PlacesController@autocomplete');
Route::get('/place/{id}',
    '\App\Http\Controllers\PlacesController@detail')->name('\App\Http\Controllers\PlacesController@detail');
Route::any('adminer', '\Miroc\LaravelAdminer\AdminerAutologinController@index')->name('adminer');
