#!/usr/bin/env bash
docker-compose -p ui42obce up -d
docker-compose -p ui42obce exec --user 1000 app /bin/bash -c "cd html && composer update"
cp ../www/html/.env.example ../www/html/.env
docker-compose -p ui42obce exec app /bin/bash -c "chown -R 1000:1000 ./html"
docker-compose -p ui42obce exec app /bin/bash -c "chmod -R 777 ./html/storage"
docker-compose -p ui42obce exec --user 1000 app /bin/bash -c "cd html && php artisan key:generate"
docker-compose -p ui42obce exec --user 1000 app /bin/bash -c "cd html && php artisan migrate"