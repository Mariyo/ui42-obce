##INSTALLATION INSTRUCTION

For install project by Docker, you must correct install docker-compose package. More info about the installation is at url: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

***Before start installation stop all XDebug listeners !!!***

#1. Create Docker containers
Execute next command:

`docker-compose -p obce up` - first start

`docker-compose -p obce up -d` - deamon start

`docker-compose -p obce down` - down project

#2. Docker commands

`docker-compose -p obce exec --user 1000 app /bin/bash`

`docker-compose -p obce build`

#3. Composer

`docker-compose -p obce exec --user 1000 app /bin/bash -c "composer create-project laravel/laravel ./html --prefer-dist && chmod 777 -R ./html/storage/"`

